#!/bin/bash

# Normal Branding
rm /etc/os-release
echo "NAME=\"AbstractOS\"" >> /etc/os-release
echo "ID=\"abstractos\"" >> /etc/os-release
echo "VERSION_ID=\"0.0.1\"" >> /etc/os-release
echo "PRETTY_NAME=\"Abstract OS 0.1\"" >> /etc/os-release
echo "HOME_URL=\"https://abstractos.org\"" >> /etc/os-release
echo "HOME_URL=\"https://bugs.abstractos.org\"" >> /etc/os-release


# Bootloader
sed -i 's/Alpine/Abstract OS/g' /boot/extlinux.conf
